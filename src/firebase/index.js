// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getAuth} from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBFiHuiw2Jqrb6tiUtIL1LbzDjlwF-HHd0",
  authDomain: "becas-y-ayudas.firebaseapp.com",
  projectId: "becas-y-ayudas",
  storageBucket: "becas-y-ayudas.appspot.com",
  messagingSenderId: "852369558374",
  appId: "1:852369558374:web:e3c8536c41fbf19d77d6a7"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export {auth}